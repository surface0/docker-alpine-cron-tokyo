# docker-alpine-cron-tokyo

Cron image based Alpine Linux with Asia/Tokyo timezone custom.

# Description

See base image [description](https://hub.docker.com/r/xordiv/docker-alpine-cron/).